﻿using System;

namespace testTask1
{
    class Program
    {

        static void Main(string[] args)
        {
            string s = Encrypt("Abcdefghij", 2) ;
            Console.WriteLine(s);
            Console.WriteLine(Decrypt(s,2));
        }

        public static string Encrypt(string originText, int n)
        {
            if (originText == null || n==0)
                return originText;

            char[] originTextArr = originText.ToLower().ToCharArray();
            char[] encryptedTextArr = new char[originText.Length];
            int count = 0;
            int i = 1;
            int j = 0;
            string encryptedString = default;

            for ( ; i < originText.Length; i += 2,j++)
            {
                encryptedTextArr[j] = originTextArr[i];
                if ((i == originTextArr.Length-1 || i == originTextArr.Length - 2) && count == 0)
                {
                    i = -2;            
                    count++;    
                }
                else if(j == encryptedTextArr.Length-1)
                {
                    n--;
                    encryptedString = new string(encryptedTextArr);
                    if (n > 0)
                    {
                        count = 0;
                        i = -1;
                        j = -1;
                        originTextArr = encryptedString.ToCharArray();
                    }
                }                 
               
            }
            return encryptedString;
        }

        public static string Decrypt(string encryptedText, int n)
        {
            if (encryptedText == null || n == 0)
                return encryptedText;

            char[] encryptedTextArr = encryptedText.ToLower().ToCharArray();
            char[] originTextArr = new char[encryptedText.Length];
            int count = 0;
            int i = 1;
            int j = 0;
            string originString = default;

            for (; i < encryptedText.Length; i += 2, j++)
            {
                originTextArr[i] = encryptedTextArr[j];
                if ((i == encryptedTextArr.Length - 1 || i == encryptedTextArr.Length - 2) && count == 0)
                {
                    i = -2;
                    count++;
                }
                else if (j == originTextArr.Length - 1)
                {
                    n--;
                    originString = new string(originTextArr);
                    if (n > 0)
                    {
                        count = 0;
                        i = -1;
                        j = -1;
                        encryptedTextArr = originString.ToCharArray();
                    }
                }

            }
            return originString;
        }

    }
}
